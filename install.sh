#!/usr/bin/sh
## Copy widget files to the user Widgets directory:

applet_path="${XDG_DATA_HOME:-$HOME/.local/share}"/plasma/plasmoids/org.kde.plasma.systemloadviewer
cp_msg="Copying applet files to "${applet_path}" ..."
if [ ! -d "${applet_path}" ]
then
    echo "${cp_msg}"
    if command -v rsync >/dev/null 2>&1; then
        rsync -av ./applets/systemloadviewer/package/ "${applet_path}"
    else
        mkdir -p "${applet_path}"
        cp -ur ./applets/systemloadviewer/package/* "${applet_path}"
    fi
    echo "Done."
else
    while true
    do
        echo "Warning: Directory "${applet_path}" exists already"
        read -rp "Do you want to overwrite files there? [Yes/No]: " yn
        case $yn in
            [Yy]*)
                echo "${cp_msg}"
                if command -v rsync >/dev/null 2>&1; then
                    rsync -av --delete ./applets/systemloadviewer/package/ "${applet_path}"
                else
                    cp -ur ./applets/systemloadviewer/package/* "${applet_path}"
                fi
                echo "Done."
                break;;
            [Nn]*)
                break;;
            *)
                echo "Please type y for yes or n for no"
        esac
    done
fi
